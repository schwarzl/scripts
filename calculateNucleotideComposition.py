__author__ = 'Thomas Schwarzl <schwarzl@embl.de>'

# quick hack to test for calculate the read compositions

# scripts reads in sequences from stdin 

"""
USAGE

echo "ACATATAGAGA\nAACACAACACA" | python calculateNucleotideComposition.py

zcat input.fastq.gz  | awk 'NR%4==2' | python calculateNucleotideComposition.py
"""

import sys


class Nucleotides:
    nucleotides = {}
    maxPosition = 0
    addedSequences = 0
    variants = {}
    
    def addSequence(self, seq):
        if seq != "":
            position = 0
            for nucleotide in seq:
                self.addNucleotide(nucleotide, position)
                position += 1
            self.addedSequences += 1
        

    def addNucleotide(self, nucleotide, position):
        if position in self.nucleotides:
            if nucleotide in self.nucleotides[position]:
                self.nucleotides[position][nucleotide] += 1
            else:
                self.nucleotides[position][nucleotide] = 1
        else:
            self.nucleotides[position] = { nucleotide : 1}
            
        self.updatePosition(position)
        self.updateNucleotides(nucleotide)
            
    def updatePosition(self, position):
        if position > self.maxPosition:
            self.maxPosition = position
    
    def updateNucleotides(self, nucleotide):
        self.variants[nucleotide] = 1
            
    def getCounts(self, position, nucleotide):
        if position in self.nucleotides and nucleotide in self.nucleotides[position]:
            return self.nucleotides[position][nucleotide]
        else:
            return 0
                
        
    def __str__(self):
        return("Nucleotide Counter: " + str(self.addedSequences) + " were added, max position is:" + str(self.maxPosition + 1))
    
    def printTable(self):
        variants = sorted(self.variants)
        print("Position\t" + "\t".join(variants))  
        
        for position in range(0, self.maxPosition):
            newline = [str(position + 1)] 
            for nucleotide in sorted(self.variants):
                newline.append(str(self.getCounts(position, nucleotide)))
            
            print("\t".join(newline))
            
        


n = Nucleotides()

for line in sys.stdin:
    line = line.strip()
    n.addSequence(line)
    
#print(n)

n.printTable()