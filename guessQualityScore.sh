#!/bin/bash
# script from medhat @ https://www.biostars.org/p/63225/


if [ $# -eq 0 ]
then
    echo "No arguments supplied. Please provide the fastq or fastq.gz file as argument"
    exit 1
fi

FILE=$1

COMMAND=""

if [[ "$FILE" == *gz ]]
then
    COMMAND="zcat $FILE | head -10000"
else
    COMMAND="head -10000 $FILE"
fi

if [ -f $FILE ];
then
    eval "$COMMAND | awk '{if(NR%4==0) printf(\"%s\",\$0);}' |  od -A n -t u1 | awk 'BEGIN{min=100;max=0;}{for(i=1;i<=NF;i++) {if(\$i>max) max=\$i; if(\$i<min) min=\$i;}}END{if(max<=74 && min<59) print \"Phred+33\"; else if(max>73 && min>=64) print \"Phred+64\"; else if(min>=59 && min<64 && max>73) print \"Solexa+64\"; else print \"Unknown score encoding\";}'"
else
	echo "Problems opening the file"
fi

