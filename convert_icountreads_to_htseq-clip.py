
import sys
i = 1
for line in sys.stdin:
	if not line.startswith("track"):
		# chrX    154276131       154276158       TCCGT_1 0       +
		x = line.split()
		start = 0
		end = 0
		if x[5] == "+":
			start = str(int(x[1]) - 1)
			end = str( x[1])
		elif x[5] == "-":
			start = str(x[2])
			end = str(int(x[2]) + 1)
		else: 
			print("ERROR")
		
		y = x[3].split("_")
		
		sys.stdout.write("\t".join([str(x[0]), str(start), str(end), "read" + str(i) + ":" + y[0] + "|" + str(int(x[2])-int(x[1])), y[1], str(x[5])]) + "\n")
		i += 1


